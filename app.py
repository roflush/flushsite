#!/usr/bin/env python3.6
import os 
from flask import Flask
from flask import render_template

app = Flask(__name__)

@app.route("/")
def index():
    return render_template('index.html')

@app.route("/blog")
def blog():
    return render_template('blog.html')

@app.route("/games")
def games():
    return render_template('games.html')

@app.route("/about")
def about():
    return render_template('about.html')

@app.route("/gpg")
def gpgkey():
    return render_template('gpg.html')

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=1024, debug=True)